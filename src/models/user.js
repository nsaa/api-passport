import argon from 'argon2';
import sqlize from "sequelize";

import sequelize from "./index.js";

/**
 * @module  User
 * @description contain the details of Attribute
 */
const { Model, DataTypes } = sqlize;

class User extends Model {
    static async isValidPassword(inputPassword, userPassword) {  
        try {
            return await argon.verify(userPassword, inputPassword);
        } catch (error) {
            throw new Error(error);
        }
        
    };
};

User.init({
    id: {
        type: DataTypes.TEXT,
        // autoIncrement: true,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING
    }
}, { sequelize, modelName: 'User', tableName: 'users', timestamps: false, createdAt: false, updatedAt: false });

User.beforeSave( async user => {
    if (!user.password) return;
    try {
        const hashedPassword = await argon.hash(user.password);
        user.password = hashedPassword;
    } catch (error) {
        throw new Error(error);
    }
});

export default User;