import sqlize from "sequelize";

const { Sequelize } = sqlize;

const DB_HOST = process.env.DB_HOST || "localhost";
const DB_NAME = process.env.DB_NAME || "nsaa-database";
const DB_PORT = process.env.DB_PORT ? parseInt(process.env.DB_PORT) : undefined;
const DB_USERNAME = process.env.DB_USERNAME || "server";
const DB_PASSWORD = process.env.DB_PASSWORD || "server";

const logging = process.env.NODE_ENV === 'developement';

const connection = new Sequelize({
    host: DB_HOST,
    port: DB_PORT || 8000,
    database: DB_NAME,
    username: DB_USERNAME,
    password: DB_PASSWORD,
    dialect: 'postgres',
    logging
});

export default connection;