import crypto from 'crypto';
import radclient from 'radclient';
import { Strategy as JWTStrategy } from "passport-jwt";
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as GithubStrategy } from 'passport-github2';
import { Strategy as GoogleStrategy } from 'passport-google-oidc';

// model
import User from "../models/user.js";

export default class PassportConfig {
    jwtSecret;
    openIdIssuer;
    radiusOptions;
    radiusSecret;
    constructor(passport) {
        this.passport = passport;
        this.jwtSecret = crypto.randomBytes(32);
        this.radiusSecret = "testing123";
        this.radiusOptions = {
            host: '192.168.56.101',
            timeout: 2000,
            retries: 3
        };
        // this.DiscoverOpenIdIssuers();
    }

    // async DiscoverOpenIdIssuers() {
    //     this.openIdIssuer = await Issuer.discover('https://accounts.google.com');
    //     console.log('Discovered issuer %s %O', this.openIdIssuer.issuer, this.openIdIssuer.metadata);
    // };

    cookieExtractor = req => {
        const token = req.cookies['access_token'];
        return token;
    };

    SetStrategy() {
        this.passport.serializeUser( (user, done) => {
            done(null, user);
        });

        this.passport.deserializeUser( (user, done) => {
            done(null, user);
        });

        this.passport.use(new JWTStrategy({ 
            jwtFromRequest: this.cookieExtractor,
            secretOrKey: this.jwtSecret
        }, async (token, done) => {
            try {
                if (!token) return done(null, false);

                const user = await User.findOne({ where: { id: token.id } });
                if(!user) return done(null, false);
                
                return done(null, user);
            } catch (error) {
                return done(error, null);
            }
        }));

        this.passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            session: false
        }, async (username, password, done) => {
            try {
                const user = await User.findOne({ where: { username } });
                if (!user) {
                    return done(null, false);
                }

                const isValid = await User.isValidPassword(password, user.password);
                if (!isValid) {
                    return done(null, false);
                }

                return done(null, user);
            } catch (error) {
                return done(error);  
            }
        }));

        this.passport.use("local-radius", new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            session: false
        }, (username, password, done) => {
            const packet = {
                code: 'Access-Request',
                secret: this.radiusSecret,
                attributes: [
                //   ['NAS-IP-Address', '192.168.1.10'],
                  ['User-Name', username],
                  ['User-Password', password]
                ]
            };

            radclient(packet, this.radiusOptions, async (err, response) => {
                if (err || !response) return done(null, false);
                if (response.code !== 'Access-Accept') return done(null, false);

                try {
                    // const user = await User.findOne({ where: { username } });
                    const [ user ] = await User.findOrCreate({ 
                        where: { username },
                        defaults: { 
                            username: username,
                            // password: password
                        },
                        returning: true, raw: true
                    });
                    if (!user) return done(null, false);

                    return done(null, user);
                } catch (error) {
                    return done(error);  
                }
            });
        }));

        this.passport.use(new GithubStrategy({
            clientID: process.env.GITHUB_CLIENT_ID,
            clientSecret: process.env.GITHUB_CLIENT_SECRET,
            callbackURL: "https://localhost:9090/auth/github/callback"
        },async (accessToken, refreshToken, profile, done) => {
            try {
                const [ user ] = await User.findOrCreate({ 
                    where: { id: profile.id },
                    defaults: { 
                        username: profile.username 
                    },
                    returning: true, raw: true
                });
                if (!profile || !user) return done(null, false);

                return done(null, user);
            } catch (error) {
                throw error;
            }
        }));

        this.passport.use(new GoogleStrategy({
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: "https://localhost:9090/auth/google/callback"
        }, async (issuer, profile, done) => {
            try {
                const [ user ] = await User.findOrCreate({ 
                    where: { id: profile.id },
                    defaults: { 
                        username: profile.displayName
                    },
                    returning: true, raw: true
                });
                if (!profile || !user) return done(null, false);

                return done(null, user);
            } catch (error) {
                throw error;
            }
        }));

    
        return this.passport;
    }
};
