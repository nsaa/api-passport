import fs from 'fs';
import path from 'path';
import https from 'https';
import dotenv from 'dotenv';

dotenv.config();

import logger from 'morgan';
import express from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import fortune from 'fortune-teller';
import session from 'express-session';
import cookieParser from 'cookie-parser';

// import connection
import connection from './models/index.js';
import User from './models/user.js';

// passport config
import PassportConfig from './config/passport-setup.js';

const app = express();
const PORT = process.env.PORT || 9000;
const HOST = process.env.DB_HOST || "localhost";
const HTTPS_PORT = process.env.HTTPS_PORT || 9090;
const passportConfig = new PassportConfig(passport);
const strategy = passportConfig.SetStrategy();
const PROTOCOL = HTTPS_PORT ? "https" : "http";

// tls
const tlsServerCrt = fs.readFileSync(path.resolve('./certs/localhost/localhost.crt'));
const tlsServerKey = fs.readFileSync(path.resolve('./certs/localhost/localhost.decrypted.key'));

// https server
const server = https.createServer({
    key: tlsServerKey,
    cert: tlsServerCrt
}, app);

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(passport.initialize());
app.use(session({
    secret: passportConfig.jwtSecret.toString('hex'),
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true, httpOnly: true }
}))
app.use(express.urlencoded({ extended: true }));

// serve files
app.use(express.static(path.resolve("src/views")));
app.use(express.static(path.resolve("src/assets")));

// ROUTING
app.get('/assets/:image', (req, res) => {
    const { image } = req.params;
    if (!image) return res.sendStatus(400);

    res.sendFile(path.resolve(`src/assets/${image}`));
});

app.get('/login', (req, res) => {
    res.sendFile(path.resolve("src/views/login.html"));
});


app.get('/signin', (req, res) => {
    res.sendFile(path.resolve("src/views/signin.html"));
});

app.get('/', 
    strategy.authenticate('jwt', { session: false, failureRedirect: '/login' }),
    async (req, res) => {
        if (!req.user) return res.sendStatus(401);
        return res.sendFile(path.resolve("src/views/adages.html"));
});

app.get('/logout', 
    strategy.authenticate('jwt', { session: false, failureRedirect: '/login' }),
    (req, res) => res.clearCookie('access_token').end()
);

// OAuth2.0 login
app.get('/auth/github', passport.authenticate('github', { scope: [ 'user:email' ] }));

app.get('/auth/github/callback', 
  passport.authenticate('github', { failureRedirect: '/login' }),
  function(req, res) {
    if (!req.user) res.sendStatus(401);
    const jwtClaims = {
        id: req.user.id,
        iss: `${HOST}:${HTTPS_PORT || PORT}`,
        aud: `${HOST}:${HTTPS_PORT || PORT}`,
        exp: Math.floor(Date.now() / 1000) + 604800,
        role: 'user',
        exam: 'Mendoza Jimenez'
    };
    
    const token = jwt.sign(jwtClaims, passportConfig.jwtSecret);

    // Successful authentication, redirect home.
    return res.status(200)
        .cookie("access_token", token, { httpOnly: true, secure: true })
        .redirect(`${PROTOCOL}://${HOST}:${HTTPS_PORT || PORT}/`);
});

// OAuth2.0 OpenID login
app.get('/auth/google', passport.authenticate('google', { scope: ["profile"] }));

app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    if (!req.user) res.sendStatus(401);
    const jwtClaims = {
        id: req.user.id,
        iss: `${HOST}:${HTTPS_PORT || PORT}`,
        aud: `${HOST}:${HTTPS_PORT || PORT}`,
        exp: Math.floor(Date.now() / 1000) + 604800,
        role: 'user',
        exam: 'Mendoza Jimenez'
    };
    
    const token = jwt.sign(jwtClaims, passportConfig.jwtSecret);

    // Successful authentication, redirect home.
    return res.status(200)
        .cookie("access_token", token, { httpOnly: true, secure: true })
        .redirect(`${PROTOCOL}://${HOST}:${HTTPS_PORT || PORT}/`);
});

app.post('/login', strategy.authenticate('local', { session: false }), 
    async (req, res) => {
        if (!req.user) return res.sendStatus(401);
        const jwtClaims = {
            id: req.user.id,
            iss: `${HOST}:${HTTPS_PORT || PORT}`,
            aud: `${HOST}:${HTTPS_PORT || PORT}`,
            exp: Math.floor(Date.now() / 1000) + 604800,
            role: 'user',
            exam: 'Mendoza Jimenez'
        };

        const token = jwt.sign(jwtClaims, passportConfig.jwtSecret);
        return res.status(200)
            .cookie("access_token", token, { httpOnly: true, secure: true })
            .json({ message: "Logged in" });
});

app.post('/login-radius', strategy.authenticate('local-radius', { session: false }), 
    async (req, res) => {
        if (!req.user) return res.sendStatus(401);
        const jwtClaims = {
            id: req.user.id,
            iss: `${HOST}:${HTTPS_PORT || PORT}`,
            aud: `${HOST}:${HTTPS_PORT || PORT}`,
            exp: Math.floor(Date.now() / 1000) + 604800,
            role: 'user',
            exam: 'Mendoza Jimenez'
        };

        const token = jwt.sign(jwtClaims, passportConfig.jwtSecret);
        return res.status(200)
            .cookie("access_token", token, { httpOnly: true, secure: true })
            .json({ message: "Logged in" });
});

app.post('/signin', async (req, res) => {
        if (!req.body) return res.sendStatus(401);
        const { username, password } = req.body;
        try {
            const result = await User.create({ username, password }, { returning: true });

            const jwtClaims = {
                id: result.id,
                iss: `${HOST}:${HTTPS_PORT || PORT}`,
                aud: `${HOST}:${HTTPS_PORT || PORT}`,
                exp: Math.floor(Date.now() / 1000) + 604800,
                role: 'user',
                exam: 'Mendoza Jimenez'
            };

            const token = jwt.sign(jwtClaims, passportConfig.jwtSecret);
            return res.status(200)
                .cookie("access_token", token, { httpOnly: true, secure: true })
                .json({ message: "Signed in" });

        } catch (error) {
            res.sendStatus(500);
            throw new Error(error);
        }
});

app.get('/fortune-teller', 
    strategy.authenticate('jwt', { session: false, failureRedirect: '/login' }),
    async (req, res) => {
        if (!req.user) return res.sendStatus(401);
        try {
            const adage = fortune.fortune();
            return res.status(200).send({ adage });

        } catch (error) {
            res.sendStatus(500);
            throw new Error(error);
        }
});

// app.listen(PORT, async () => {
//     try {
//         await connection.authenticate();
//         console.log("Database connected");

//         await connection.sync({ force: true });
//         console.log("Databases synced successfully");
//     } catch (error) {
//         throw error;
//     }

//     console.log(`HTTP Server listenning on port ${PORT}`);

// });

server.listen(HTTPS_PORT, async () => {

    try {
        await connection.authenticate();
        console.log("Database connected");

        await connection.sync({ force: true });
        console.log("Databases synced successfully");
    } catch (error) {
        throw error;
    }

    console.log(`HTTPS Server listenning on port ${HTTPS_PORT}`);
});

